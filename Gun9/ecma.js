var sepet = [];
var ekleme_formu = document.getElementById('form');

ekleme_formu.addEventListener("submit", function (olay) {
    olay.preventDefault(); // sayfanın submit işlemi sonucu sayfa yenilemensini engelliyor.
    var isim = document.getElementsByName("urun-adi");
    var fiyat = document.getElementsByName("urun-fiyat");
    var yeni_urun = {
      isim:isim[0].value,
      fiyat:fiyat[0].value
    }
    sepet.push(yeni_urun);
    yazdir();
});

function yazdir(){
  var liste = document.getElementById('liste');
  liste.innerHTML = "";
  sepet.forEach(function(obje){
    var li = document.createElement("li");
    li.innerHTML = "Ürün Adı : " +  obje.isim + " Fiyatı : " + obje.fiyat;
    liste.appendChild(li);
  });
};
