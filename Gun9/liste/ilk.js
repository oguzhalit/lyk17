var liste = [
  {
    id : 1,
    isim : "html",
    durum : false
  },
  {
    id : 2,
    isim : "css",
    durum : false
  },
  {
    id : 3,
    isim : "ecmascript6",
    durum : true
  },
  {
    id : 4,
    isim : "jQuery",
    durum : true
  },
  {
    id : 5,
    isim : "python",
    durum : true
  }
];


liste.forEach(function(obje){
  if(obje.durum == false){
    $("#yapilacak").append("<li>" + obje.isim + "</li>");
  }else{
    $("#yapildi").append("<li>" + obje.isim +"</li>");
  }
});
